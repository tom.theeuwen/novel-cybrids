Starting point is the file named "chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced.recode.vcf"

1. Convert into a .bed .bim and .fam files (using plink)
plink --vcf chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced.recode.vcf --allow-extra-chr --make-bed --recode --out chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced

2. Add the phenotypes to the .fam file (using an R script) and use the "Per_genotypeAdditive_Emmeans.csv" file.
Rscript 2. Phenotypes to fam.R

3. Generate a matrix
gemma -bfile chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced -gk 1 -o chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced_full_kinship

4. Run GEMMA
gemma --bfile chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced -k /output/gemma --bfile chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced -k chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced_full_kinship.cXX.txt -lmm 1 -maf 0 -n 614 -o chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced_MAF_0_614

seq 1986 | parallel -j 20 'gemma -bfile chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced -maf 0 -n {} -k chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced_full_kinship.cXX.txt -lmm 1 -o chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced_MAF_0_{}'

seq 1986 | parallel -j 20 'gemma -bfile chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced -maf 0.04 -n {} -k chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced_full_kinship.cXX.txt -lmm 1 -o chrPt_Complete_60_cybrids_QD25_filtred_pass_50%_Heterozygous_pipes_replaced_MAF_0.04_{}'

5. Manhattan plots and summary LOD score files.
Rscript 3. Manhattan Plots.R
  