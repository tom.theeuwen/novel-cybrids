library(rworldmap)
library(rworldxtra)
library(ggplot2)
library(mapproj)

setwd("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid population/Plasmotype analysis/chrPt/")

locations <- read.table("World accessions cleaned public accessions.csv", sep = ",", header=T)
locations <- na.omit(locations)
locations$Group <- as.factor(locations$Group)
locations$X4_clusters <- as.factor(locations$X4_clusters)
locations$Latitude <- as.numeric(locations$Latitude)
locations$Longitude <- as.numeric(locations$Longitude)

data("countryExData", envir = environment(), package = "rworldmap")
mymap <- joinCountryData2Map(countryExData,
                             joinCode = "ISO3",
                             nameJoinColumn = "ISO3V10",
                             mapResolution = "high")
mymap <- fortify(mymap)

mypal = rainbow(20)

g <- ggplot() + 
  geom_path(data = mymap, aes(long, lat, group = group), 
            color = "black", size=0.5) + 
  geom_point(data = locations, aes(x = Longitude, y = Latitude, fill=Group, colour=Group, shape=X4_clusters), size = 1.5) + 
  scale_shape_manual(values=c(15, 16, 17, 18)) +
  scale_color_manual(values = mypal) +
  scale_x_continuous(limits = c(-160, 200)) +
  ylim(c(-35, 70)) + 
  xlim(c(-24, 140)) + 
  ylab("Latitude") + 
  xlab("Longitude") + 
  theme_bw() + theme(panel.border = element_blank(), panel.grid.major = element_blank(),
                     panel.grid.minor = element_blank(), axis.line = element_line(colour="black"),
                     legend.position="none", axis.text = element_text(colour="black"))
plot(g)

png("Mapping World Zoomed Accessions Public accessions k=20 groups_black lines_shapes_thick.png", width = 14/1.7, height = 9/1.7, units = 'in', res = 300)
plot(g)
dev.off()

#svg("Mapping World Zoomed Accessions Public accessions k=20 groups_black lines.svg", width = 9, height = 5.5)
#plot(g)
#dev.off()