library(tibble)
library(ggplot2)
library(patchwork)

if(Sys.info()["user"] == "tomtheeuwen"){
  raw.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PD_Year_1/New cybrids manuscript/Data/3. Cybrids in dynamic environments/Phenovator/")
  out.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/Phenovator_Exps_2021/Analysis 2 repeated for publishing/")
}

setwd(raw.dir)

##Output data file
p2of <- file.path(out.dir, "Working_outliers_removed_on_leaf_area_whole_pop_1.5sd_nocvi_per_genotype.csv")
##Output histogram file
p2hf <- file.path(out.dir, "Working_outliers_removed_on_leaf_area_whole_pop_1.5sd_nocvi_per_genotype.pdf")
###first column with phenotypes
start_pheno <- 22
###specify column on which to do outlier removal
pheno_outlier <- 323

##This should work on the basis given above.

data <- read.table("TTI_All_Genotypes_PAM puls_dFFm_Cybrids_1_2_3_Col_nocvi.csv", sep = ",", header = TRUE) 
old_data <- data

pheno_outlier_name <- colnames(data[pheno_outlier])

before_gg <- ggplot(data, aes_string(x=pheno_outlier_name)) + 
  geom_histogram()
before_gg

data$Cybrid <- as.factor(data$Cybrid)
genotypes <- levels(data$Cybrid)
#View(genotypes)
decision_compiler <- NULL
new_data <- NULL

geno <- genotypes[1]
for(geno in genotypes){
  print(geno)
  geno_data <- data[which(data$Cybrid == geno),] 
  size <- as.numeric(geno_data[,pheno_outlier])
  st.dev <- sd(size, na.rm=TRUE)
  mean <- mean(size, na.rm=TRUE)
  min_threshold <- mean - 1.5*st.dev
  max_threshold <- mean + 1.5*st.dev
  i <- 1
  for(i in 1:nrow(geno_data)){
    size_plant <- geno_data[,pheno_outlier][i]
    if(is.na(size_plant)){
      decision <- "Remove"
    } else {
      if(size_plant < min_threshold){
        decision <- "Remove"
      } else {
        decision <- "Keep"
      }
    }
    decision_compiler <- rbind(decision_compiler,decision)
    if(decision == "Remove"){
      geno_data[i,start_pheno:(ncol(geno_data))] <- replace(geno_data[i,start_pheno:(ncol(geno_data))],,NA)
    }
  }
  new_data <- rbind(new_data,geno_data)
}
new_data <- add_column(new_data,decision_compiler, .before = start_pheno)

new_data <- new_data[order(new_data$Experiment, new_data$Replicate, new_data$Table_pos, new_data$Image_pos),]
old_data <- old_data[order(old_data$Experiment, old_data$Replicate, old_data$Table_pos, old_data$Image_pos),]
old_data <- add_column(old_data,new_data$decision_compiler, .before = start_pheno)

after_gg <- ggplot(old_data, aes_string(x=pheno_outlier_name, color="new_data$decision_compiler")) + 
  geom_histogram(fill="white")
after_gg

pdf(p2hf)
plot(before_gg/after_gg)
dev.off()
before_gg/after_gg

write.csv(new_data,p2of,row.names=F)


