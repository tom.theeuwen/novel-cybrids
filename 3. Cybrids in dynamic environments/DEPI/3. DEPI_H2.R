library(lme4)
library(splitstackshape)
library(dplyr)
library(purrr)

if(Sys.info()["user"] == "tomtheeuwen"){
  raw.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/DEPI_Exps_2021/Analysis 2 repeated for publishing/")
  out.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/DEPI_Exps_2021/Analysis 2 repeated for publishing/")
} 

setwd(raw.dir)
###Input file
data <- read.table("Working_outliers_removed_on_FvFm_whole_pop_1.5sd_clean.csv", sep = ",", header = TRUE) 
data <- data[-which(data$Plasmotype == "Ely"),]
data <- data[-which(data$Plasmotype == "WT"),]

length(levels(as.factor(data$Plasmotype)))

#Function for calculating heritabilities
heritability <- function(i){
  Heritability <- NULL
  phenotype <- colnames(data)[i]
  colm <- i
  variable <- data[,colm]
  #Estimate variance componence
  fitlmer_rand <- lmer(variable ~  (1|Nucleotype) + (1|Plasmotype) + (1|Nucleotype:Plasmotype) + (1|experiment) + (1|camera), data = data, REML=TRUE)
  out <- as.data.frame(VarCorr(fitlmer_rand))
  
  header <- out$grp
  number_of_components <- length(out$vcov)
  total_variance <- sum(out$vcov)
  for(comp in 1:number_of_components){
    H2 <- out$vcov[comp]/total_variance
    Heritability <- cbind(Heritability,H2)
  }
  colnames(Heritability) <- header
  return(Heritability)
  
}

phenotypes <- colnames(data[c(15:ncol(data))])

H2 <- map(c(15:ncol(data)),heritability)

H2.df <- as.data.frame(do.call(rbind, H2))
H2.df <- cbind(phenotypes, H2.df)

H2.df <- cSplit(H2.df, "phenotypes" , "_")
H2.df <- H2.df[,c(8,9,1,2,3,4,5,6)]
H2.df$Genetic <- H2.df$`Nucleotype:Plasmotype` + H2.df$Plasmotype + H2.df$Nucleotype
H2.df$Nuc_Plas <- H2.df$`Nucleotype:Plasmotype`/H2.df$Genetic
H2.df$Plas <- H2.df$Plasmotype/H2.df$Genetic
H2.df$Nuc <- H2.df$Nucleotype/H2.df$Genetic

setwd(out.dir)
colnames(H2.df)[1] <- "Phenotype"
colnames(H2.df)[2] <- "Time"

##summary statistics
fraction_H2 <- H2.df[,c(1,2,9,10,11,12)]

fraction_H2$H2_too_low <- "High_enough"
H2.df$H2_too_low <- "High_enough"
for(row in 1:nrow(fraction_H2)){
  if(fraction_H2$Genetic[row]<0.05){
    fraction_H2$H2_too_low[row] <- "Too_low"
    H2.df$H2_too_low[row] <- "Too_low"
    fraction_H2$Nuc_Plas[row] <- NA
    fraction_H2$Plas[row] <- NA
    fraction_H2$Nuc[row] <- NA
  }
}

write.csv(H2.df, "Additive_Heritability.csv", row.names = F)

# Function to calculate mean of specified columns
calculate_means <- function(data, value_cols) {
  data %>%
    summarise(across(all_of(value_cols), mean, na.rm = TRUE))
}

# Specify the value columns
value_columns <- c("Genetic", "Nuc_Plas", "Plas", "Nuc")

# Calculate the means
result_overall <- calculate_means(fraction_H2, value_columns)
result_overall
write.csv(result_overall, "Means_H2_and_Fraction_H2_over_all_days.csv", row.names = F)

#per phenotype over all days
# Function to calculate mean of specified columns based on group column
calculate_group_means <- function(data, group_col, value_cols) {
  data %>%
    group_by(!!sym(group_col)) %>%
    summarise(across(all_of(value_cols), mean, na.rm = TRUE))
}

# Specify the group column and value columns
group_column <- "Phenotype"
value_columns <- c("Genetic", "Nuc_Plas", "Plas", "Nuc")

# Calculate the means
result_overall <- calculate_group_means(fraction_H2, group_column, value_columns)
write.csv(result_overall, "Means_H2_and_Fraction_H2_per_phenotype_over_all_days.csv", row.names = F)

#per phenotype per day
fraction_H2$day <- "empty"
row <- 1
for(time in fraction_H2$Time){
  if(time < 24){
    fraction_H2$day[row] <- "Stable light day 1"
  }
  if(time >= 24){
    fraction_H2$day[row] <- "Fluctuating light day 1"
  }
  if(time >= 48){
    fraction_H2$day[row] <- "Stable day 2"
  }
  if(time >= 72){
    fraction_H2$day[row] <- "Fluctuating light day 2"
  }
  if(time >= 96){
    fraction_H2$day[row] <- "Stable light cold day"
  }
  if(time >= 120){
    fraction_H2$day[row] <- "Fluctuating light cold day"
  }
  row <- row +1 
}

# Function to calculate mean of specified columns based on two grouping columns
calculate_group_means2 <- function(data, group_cols, value_cols) {
  data %>%
    group_by(across(all_of(group_cols))) %>%
    summarise(across(all_of(value_cols), mean, na.rm = TRUE))
}

# Specify the group column and value columns
group_columns <- c("Phenotype", "day")
value_columns <- c("Genetic", "Nuc_Plas", "Plas", "Nuc")

# Calculate the means
result <- calculate_group_means2(fraction_H2, group_columns, value_columns)
result

write.csv(result, "Means_H2_and_Fraction_H2_per_phenotype_per_day.csv", row.names = F)
