library(dplyr)
library(tidyr)
library(stringr)
library(reshape2)
library(splitstackshape)

if(Sys.info()["user"] == "tomtheeuwen"){
  raw.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PD_Year_1/New cybrids manuscript/Data/3. Cybrids in dynamic environments/DEPI/Raw_data_DEPI/")
  geno.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PD_Year_1/New cybrids manuscript/Data/3. Cybrids in dynamic environments/DEPI/")
  out.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/DEPI_Exps_2021/Analysis 2 repeated for publishing/")
}

setwd(raw.dir)
list_of_files <- list.files()[c(8:19)]

cleandata <- NULL
count <- 1
file <- "Cybrid_allqit.txt"
for(file in list_of_files){
  cleandata_temp <- read.table(file, sep="\t",
                         header=TRUE, check.names=FALSE)%>%
    #mutate functie op lux1 en lux226 een spatie te geven
    separate(col= "name[position][flat][experiment][camera][replicate]",
             into = paste(c("name","position", "empty",
                            "flat","empty2", "experiment","empty3",
                            "camera","empty4", "replicates")),
             sep = "[\\[\\]]") #%>%
             #sep = "[^[:alnum:][:punct]]") %>%
    #select(-empty,-empty2,-empty3,-empty4)
    #unite("genotype",1:2, sep="-") %>%
    #unite("date",5:7, sep="-")
  cleandata_temp <- cleandata_temp[,c(1:2,4,6,8,10:ncol(cleandata_temp))]
  ##add phenotype names to timepoints
  names <- data.frame(as.character(data.frame(str_split(file,"[.]"))[1,]), cbind(colnames(cleandata_temp)[7:ncol(cleandata_temp)]))
  new <- unite(names, newcol, 1:2, remove = F)[,1]
  colnames(cleandata_temp) <- c(colnames(cleandata_temp)[1:6], new) 
  
  if(count == 1){
    cleandata <- cbind(cleandata, as.matrix(cleandata_temp))
  } else {
    cleandata <- cbind(cleandata, as.matrix(cleandata_temp[7:ncol(cleandata_temp)]))
  }
  count <- count + 1
  print(file)
}

Horizontaldata <- data.frame(cleandata)
rm(cleandata)
rm(cleandata_temp)

Horizontaldata <- Horizontaldata[-which(Horizontaldata$name == "*light_intensity"),]
Horizontaldata <- Horizontaldata[-which(Horizontaldata$name == "null"),]
Horizontaldata$name <- factor(Horizontaldata$name)
Horizontaldata[Horizontaldata == "?"] <- "NA"
Horizontaldata <- cSplit(Horizontaldata, "position", sep = "0")
Horizontaldata <- subset(Horizontaldata, select=c(1:2,(ncol(Horizontaldata)-1), (ncol(Horizontaldata)), 3:(ncol(Horizontaldata)-2)))
Horizontaldata <- cSplit(Horizontaldata, "name", sep = "Tom_")
Horizontaldata <- subset(Horizontaldata, select=c(ncol(Horizontaldata), 1:(ncol(Horizontaldata)-2)))
colnames(Horizontaldata) <- c("ID", colnames(Horizontaldata)[2:ncol(Horizontaldata)])

##merge with genotype names
setwd(geno.dir)
geno <- read.csv("Cybrid_name_to_ID.csv", header = TRUE)
merged <- dplyr::right_join(Horizontaldata,geno, by="ID")
merged <- subset(merged, select=c(1, (ncol(merged)-(ncol(geno)-1)+1):ncol(merged), 2:(ncol(merged)-ncol(geno)+1)))

setwd(out.dir)  
write.csv(merged, "Cybrid_allpheno_cleaned_Horizontal_merged.csv", row.names=F)
