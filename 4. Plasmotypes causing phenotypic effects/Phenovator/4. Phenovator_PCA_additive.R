library(ggfortify)
library(ggplot2)
library(splitstackshape)
library(ggrepel)

if(Sys.info()["user"] == "tomtheeuwen"){
  raw.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/Phenovator_Exps_2021/Analysis 2 repeated for publishing/")
  out.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/Phenovator_Exps_2021/Analysis 2 repeated for publishing/")
}

setwd(raw.dir)

data_emmean <- read.csv("Phenovator_additive_BLUEs.csv", header=T, row.names = 1)
data_H2 <- read.csv("Additive_Heritability.csv", header=T)
data_colours <- read.csv("PCA_colours.csv", header = T)

plasmotypes <- row.names(data_emmean)
data_emmean <- data.frame(t(data_emmean))
data_emmean$H2 <- data_H2$Genetic
data_emmean <- data_emmean[-which(data_emmean$H2 < 0.05),]
data_emmean <- data_emmean[-ncol(data_emmean)]
data_emmean <- t(data_emmean)
row.names(data_emmean) <- plasmotypes
data_emmean <- data.frame(data_emmean)
data_emmean$Colour <- data_colours$PCA.colour
data_emmean <- data_emmean[complete.cases(data_emmean),] #to remove missing values

colours <- data_emmean$Colour
data_emmean <- data_emmean[,-ncol(data_emmean)]

# Set output folder
setwd(out.dir)

## extract matrix
matrix <- as.matrix(data_emmean)

# prepare hierarchical cluster
hc <- hclust(dist(matrix))

###################calculate PCAs
pca <- prcomp(matrix)

cbPalette <- c("#000000", "#E69F00", "#0072B2", "#009E73", "#56B4E9", "#D55E00", "#CC79A7")

var_explained <- pca$sdev^2/sum(pca$sdev^2)
new_plot_pc1_pc2 <- ggplot(data = pca, aes(x=PC1,y=PC2)) +
  geom_point(size=1) +
  theme_bw(base_size=12) + 
  labs(x=paste0("PC1: ",round(var_explained[1]*100,1),"%"),
       y=paste0("PC2: ",round(var_explained[2]*100,1),"%")) +
  theme(legend.position="top") +
  geom_text_repel(label = rownames(data_emmean), size = 2) 

new_plot_pc1_pc2_1 <- autoplot(pca, data=data_emmean, colour = colours) +
  geom_text_repel(label=rownames(data_emmean),size=3, colour = colours) +
  theme_bw()

png(paste("Phenovator_additive_pca_colours_wide.png",sep="_"), width = 9/1.5, height = 6/1.5, units = 'in', res = 300)
print(new_plot_pc1_pc2_1)
dev.off()
