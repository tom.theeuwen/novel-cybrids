library(ggplot2)
library(patchwork)
library(scales)
library(reshape)
library(dplyr)


if(Sys.info()["user"] == "tomtheeuwen"){
  raw.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/Phenovator_Exps_2021/Analysis 2 repeated for publishing/")
  out.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/Phenovator_Exps_2021/Analysis 2 repeated for publishing/")
}

data_emmean <- read.csv("Phenovator_interaction_BLUEs.csv", header=T)
data_SE <- read.csv("Phenovator_interaction_SE.csv", header=T)

start.pheno <- 3
stop.pheno <- ncol(data_emmean)
setwd(out.dir)

i <- 3
for(i in start.pheno:stop.pheno){
  phenotype <- colnames(data_emmean)[i]
  #data_emmean$Nucleotype <- as.factor(data_emmean$Nucleotype)
  Nucleotypes <- levels(as.factor(data_emmean$Nucleotype))
  for(nucleotype in Nucleotypes){
    data_emmean_nucleotype <- data_emmean[which(data_emmean$Nucleotype == nucleotype),]
    emmean <- data_emmean_nucleotype[,i]
    data_SE_nucleotype <- data_SE[which(data_SE$Nucleotype == nucleotype),]
    SE <- data_SE_nucleotype[,i]
    data <- as.data.frame(cbind(data_emmean_nucleotype$Plasmotype, data_emmean_nucleotype$Nucleotype, emmean, SE))
    colnames(data)[c(1,2)] <- c("Plasmotype", "Nucleotype")
    data$Nucleotype <- as.factor(data$Nucleotype)
    data$emmean <- as.numeric(data$emmean)
    data$SE <- as.numeric(SE)
    plot <- ggplot(data=data, aes(x=Plasmotype, y=emmean, fill=emmean)) +
      geom_col() +
      scale_fill_gradient(low="#22FF00", high="#FF0000") +
      ylab(phenotype) +
      xlab(NULL) +
      geom_errorbar(aes(ymin=emmean-SE, ymax=emmean+SE), width=0.5) +
      theme(axis.title.x=element_blank(),
            axis.text.x=element_blank(),
            axis.ticks.x=element_blank())
      #theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
        
    if(nucleotype == "Bur"){
      plot1 <- plot +
        ggtitle("Bur Nucleotype")
    } else if(nucleotype == "Col"){
      plot2 <- plot +
        ggtitle("Col-0 Nucleotype")
    } else if(nucleotype == "Cvi"){
      plot3 <- plot +
        ggtitle ("Cvi Nucleotype")
    } else if(nucleotype == "Tanz"){
      plot4 <- plot +
        theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
        ggtitle("Tanz Nucleotype")
    }
  }
  
  png(paste("Phenovator", phenotype,"heatmap.png",sep="_"), width = 16, height = 8, units = 'in', res=300)
  print(plot1/plot2/plot3/plot4)
  dev.off()
}
