library(lme4)
library(emmeans)
library(tidyr)
library(dplyr)
library(purrr)
library(furrr)
library(pbkrtest)

if(Sys.info()["user"] == "tomtheeuwen"){
  raw.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/DEPI_Exps_2021/Analysis 2 repeated for publishing/")
  out.dir <- file.path("/Users/tomtheeuwen/Library/CloudStorage/OneDrive-WageningenUniversity&Research/PhD_Year_4/New cybrid phenotyping/Data Analysis Cybrids/DEPI_Exps_2021/Analysis 2 repeated for publishing/")
} 

setwd(raw.dir)
###Input file
data <- read.table("Working_outliers_removed_on_FvFm_whole_pop_1.5sd_clean.csv", sep = ",", header = TRUE) 
data <- data[-which(data$Plasmotype == "Ely"),]
#data <- data[-which(data$Plasmotype == "WT"),] This is not removed here, since the WT need to be analysed within each nucleotype

length(levels(as.factor(data$Plasmotype)))
emmean_df <- NULL
SE_df <- NULL

i <- 15
cybrid.emmeans <- function(i){
  phenotype <- colnames(data)[i]
  colm <- i
  variable <- data[,colm]
  
  #normal ANOVA
  fitlmer_fix <- lmer(variable ~  Nucleotype + Plasmotype + Nucleotype * Plasmotype + (1|experiment) + (1|camera), data = data, REML=TRUE)
  em.cybrid <- emmeans::emmeans(fitlmer_fix, ~ Nucleotype*Plasmotype, mode = "kenward-roger", adjust = 'none')
  em.cybrid <- data.frame(em.cybrid)
  nucleotype <- data.frame(em.cybrid$Nucleotype)
  plasmotype <- data.frame(em.cybrid$Plasmotype)
  names <- cbind(nucleotype, plasmotype)
  emmean_df <- cbind(emmean_df, em.cybrid$emmean)
  SE_df <- cbind(SE_df, em.cybrid$SE)
  
  return(list(emmean_df = emmean_df, SE_df = SE_df, names = names))
}

phenotypes <- colnames(data[c(15:2000)])
plan(multisession)
results <- future_map(c(15:2000), cybrid.emmeans)
#emmeans.cybrid <- cybrid.emmeans(15)
# Combine the results
emmean_dfs <- lapply(results, function(x) x$emmean_df)
SE_dfs <- lapply(results, function(x) x$SE_df)
names_out <- lapply(results, function(x) x$names)

combined_emmean_df <- do.call(cbind, emmean_dfs)
combined_SE_df <- do.call(cbind, SE_dfs)
combined_names <- do.call(cbind, names_out)

combined_emmean_df <- cbind(combined_names[,c(1:2)], combined_emmean_df)
combined_SE_df <- cbind(combined_names[,c(1:2)], combined_SE_df)

colnames(combined_emmean_df) <- c("Nucleotype", "Plasmotype", phenotypes)
colnames(combined_SE_df) <- c("Nucleotype", "Plasmotype", phenotypes)
  
setwd(out.dir)
write.csv(combined_emmean_df, "DEPI_interaction_BLUEs.csv", row.names = F)
write.csv(combined_SE_df, "DEPI_interaction_SE.csv", row.names = F)


