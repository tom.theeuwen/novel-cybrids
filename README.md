# Novel Cybrids
These directories contain the code used in the analyses for the Theeuwen et al., 2024 preprint entitled "Species wide inventory of Arabidopsis thaliana organellar variation reveals ample phenotypic variation for photosynthetic performance".

The results are divided into 5 seperate sections, each has a dedicated directory containing the scripts. These are R scripts and txt files with linux commands. 

The data called on in the scripts is available via Zenodo, 10.5281/zenodo.11259865. The only file missing is the VCF file of the nuclear variants for the 1531 A. thaliana accessions. The size exceeds the quota of Zenodo. I will happily share it via a different way if you reach out to me on tom.theeuwen@wur.nl.

